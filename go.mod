module my-site

go 1.18

require (
	github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy-plugin-netlify v1.0.1-0.20230618221459-92cc5a271005
	github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy-plugin-netlify-cms v1.0.1-0.20230618221459-92cc5a271005
	github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy-plugin-reveal v0.0.0-20230618221459-92cc5a271005
	github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy/v5 v5.7.1-0.20230618221459-92cc5a271005
)

require (
	github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy-core v0.2.0 // indirect
	github.com/wowchemy/wowchemy-hugo-themes/modules/wowchemy-seo v0.1.0 // indirect
)
