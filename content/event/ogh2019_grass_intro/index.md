---
abstract: GRASS GIS is a general purpose Free and Open Source geographic 
 information system (GIS) that offers raster, 3D raster and vector data 
 processing support. In this session we will start with a few GRASS GIS 
 basics, showing its concepts. We then focus on analysing the ECA&D data
 which is a European climatic data time series of daily temporal resolution. 
 Using it we look at the data organization and perform data aggregation in 
 GRASS GIS, along with univariate statistics, raster map algebra, and zonal
 statistics. This part is followed by classification with machine learning 
 (RandomForest Classifier) and concluded with a quick glance at linear and 
 multiple regression in GRASS GIS.
all_day: false
authors: [Veronica Andreo and Markus Neteler]
date: "2019-09-03T11:00:00Z"
date_end: "2019-09-03T13:00:00Z"
event: OpenGeoHub Summer School 2019
event_url: https://opengeohub.org/summer_school_2019
featured: true
image:
  caption: 'TBD'
  focal_point: Right
links:
- icon: twitter
  icon_pack: fab
  name: Follow
  url: https://twitter.com/VeronicaAndreo
location: Muenster, Germany
math: true
projects: ""
publishDate: ""
slides: ""
summary: Intro to GRASS GIS workshop at OpenGeoHub Summer School 2019 together with Markus Neteler.
tags: ["GRASS GIS", "remote sensing", "open source"]
title: Introduction to GRASS GIS
url_code: "https://neteler.gitlab.io/grass-gis-analysis/"
url_pdf: ""
url_slides: "https://gitpitch.com/veroandreo/grass_opengeohub2019/master?p=slides/intro"
url_video: "https://www.youtube.com/watch?v=eL4M6OCvAys&list=PLXUoTpMa_9s1npXD6S9M0_2pUgnTd6cqV&index=6"
---

<!---
{{% callout note %}}
Click on the **Slides** button above to view the built-in slides feature.
{{% /callout %}}

Slides can be added in a few ways:

- **Create** slides using Academic's [*Slides*](https://sourcethemes.com/academic/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the event file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the event file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://sourcethemes.com/academic/docs/writing-markdown-latex/).

Further event details can easily be added to this page using *Markdown* and $\rm \LaTeX$ math code.
--->
