---
abstract: GRASS GIS is a general purpose Free and Open Source geographic information system
 (GIS) that offers raster, 3D raster and vector data processing support. Since 2015, GRASS 
 GIS has also oficially incorporated a powerful support for time series (TGRASS). Through 
 this, GRASS GIS became the first open source temporal GIS with comprehensive 
 spatio-temporal analysis, processing and visualization capabilities. This functionality 
 makes it easy to manage, analyse and visualize for example climatic data, vegetation index
 time series, harvest data or landuse changes over time. Time series are handled through
 new data types called space time data sets (stds) which are used as input in TGRASS
 modules. In this way, TGRASS simplifies the processing and analysis of large time series
 of hundreds of thousands of maps. For example, users can aggregate a daily time series 
 into a monthly time series in just one line; get the date per year in which a certain 
 value is reached; select maps from a time series in time periods in which a different time 
 series reaches a maximum value, perform different temporal as well as spatial operations 
 among time series, and so much more.

 In this session we will explore the combined use of GRASS GIS and R. We will use a daily 
 time series of LST (thanks to [mundialis](https://www.mundialis.de/en/)) to extract 
 relevant environmental variables for a mosquito species that transmits West Nile virus in 
 Northern Italy. Particularly, we will use TGRASS to estimate bioclimatic variables such as 
 those from Worldclim, start, end and length of mosquito season, number of consecutive days 
 with a certain LST value, number of potential mosquito generations, etc. We will then 
 import our vector and raster maps into R and proceed with the modeling and prediction.

 All the material is available at  
 [https://github.com/veroandreo/grass_opengeohub2019](https://github.com/veroandreo/grass_opengeohub2019).
all_day: false
authors: [Veronica Andreo]
date: "2019-09-05T11:00:00Z"
date_end: "2019-09-05T13:00:00Z"
event: OpenGeoHub Summer School 2019
event_url: https://opengeohub.org/summer_school_2019
featured: true
image:
  caption: 'Image credit: Santiago Rodriguez'
  focal_point: Right
links:
- icon: twitter
  icon_pack: fab
  name: Follow
  url: https://twitter.com/VeronicaAndreo
location: Muenster, Germany
math: true
projects: ""
publishDate: ""
slides: ""
summary: Workshop at OpenGeoHub Summer School 2019 about the analysis of space-time satellite data for disease ecology applications with GRASS GIS and R stats.
tags: ["GRASS GIS", "R", "time series", "remote sensing", "disease ecology", "MaxEnt"]
title: Analyzing space-time satellite data for disease ecology applications with GRASS GIS and R stats
url_code: ""
url_pdf: ""
url_slides: "https://gitpitch.com/veroandreo/grass_opengeohub2019/master?p=slides/tgrass_rstats_disease_ecology"
url_video: "https://www.youtube.com/watch?v=nu_ZFvmAFGw&list=PLXUoTpMa_9s1npXD6S9M0_2pUgnTd6cqV&index=14"
---

<!---
{{% callout note %}}
Click on the **Slides** button above to view the built-in slides feature.
{{% /callout %}}

Slides can be added in a few ways:

- **Create** slides using Academic's [*Slides*](https://sourcethemes.com/academic/docs/managing-content/#create-slides) feature and link using `slides` parameter in the front matter of the event file
- **Upload** an existing slide deck to `static/` and link using `url_slides` parameter in the front matter of the event file
- **Embed** your slides (e.g. Google Slides) or presentation video on this page using [shortcodes](https://sourcethemes.com/academic/docs/writing-markdown-latex/).

Further event details can easily be added to this page using *Markdown* and $\rm \LaTeX$ math code.
--->
