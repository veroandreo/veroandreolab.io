+++
# A Skills section created with the Featurette widget.
widget = "featurette"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 30  # Order that this section will appear.

title = "Skills"
subtitle = ""

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# For available icons, see: https://sourcethemes.com/academic/docs/widgets/#icons

[[feature]]
  icon = "satellite"
  icon_pack = "fas"
  name = "Remote sensing"
  description = ""

[[feature]]
  icon = "layer-group"
  icon_pack = "fas"
  name = "GIS"
  description = ""
  
[[feature]]
  icon = "r-project"
  icon_pack = "fab"
  name = "R"
  description = ""
  
[[feature]]
  icon = "chart-line"
  icon_pack = "fas"
  name = "Statistics"
  description = ""  
  
[[feature]]
  icon = "laptop-code"
  icon_pack = "fas"
  name = "Coding"
  description = ""
  
[[feature]]
  icon = "overleaf"
  icon_pack = "ai"
  name = "Scientific writing"
  description = ""
  
+++
