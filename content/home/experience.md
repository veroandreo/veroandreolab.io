+++
# Experience widget.
widget = "experience"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 60  # Order that this section will appear.

title = "Experience"
subtitle = ""

[content]
  count = 3

[design]
  view = 2

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "Jan 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Associate Researcher"
  company = "CONICET"
  company_url = "https://www.conicet.gov.ar/"
  location = "Córdoba, Argentina"
  date_start = "2023-11-01"
  date_end = ""
  description = """
  Responsibilities include:
  
  * Research and securing funding 
  * Supervision of MSc and PhD students
  * Teaching
  """

[[experience]]
  title = "Assistant Researcher"
  company = "CONICET"
  company_url = "https://www.conicet.gov.ar/"
  location = "Córdoba, Argentina"
  date_start = "2018-07-01"
  date_end = "2023-10-31"
  description = """
  Responsibilities include:
  
  * Research on the environmental drivers of vector-borne diseases
  * Supervision of MSc and PhD students
  * Teaching
  """
  
[[experience]]
  title = "Postdoc researcher"
  company = "ITC - University of Twente"
  company_url = "https://www.itc.nl/"
  location = "Enschede, The Netherlands"
  date_start = "2016-07-01"
  date_end = "2018-06-30"
  description = """
  Responsibilities include:
  
  * Research in image analysis for Health Geography
  * Supervision of MSc and PhD students
  """

+++

<!---
[[experience]]
  title = "Consultant"
  company = "CONAE"
  company_url = ""
  location = "Cordoba, Argentina"
  date_start = "2016-02-01"
  date_end = "2016-06-30"
  description = """Derived remote sensing products for the prediction 
  of Chagas disease risk of infection."""

[[experience]]
  title = "GIS technician"
  company = "Fondazione Edmund Mach"
  company_url = ""
  location = "San Michelle al'Adige, Italy"
  date_start = "2015-05-01"
  date_end = "2015-07-31"
  description = """Conducted research on the environmental variables 
  related to the distribution and abundance of ticks in Central 
  Europe."""

[[experience]]
  title = "Consultant"
  company = "UNC"
  company_url = ""
  location = "Cordoba, Argentina"
  date_start = "2015-09-01"
  date_end = "2015-12-31"
  description = """Implemented algorithms for the determination of 
  flooded areas and risk of flooding through analysis of satellite time 
  series."""

[[experience]]
  title = "Consultant"
  company = "CONAE"
  company_url = ""
  location = "Cordoba, Argentina"
  date_start = "2014-08-01"
  date_end = "2014-12-31"
  description = """Analysis of ocean color remote sensing time 
  series."""

[[experience]]
  title = "Consultant"
  company = "UNC"
  company_url = ""
  location = "Cordoba, Argentina"
  date_start = "2014-09-09"
  date_end = "2014-10-31"
  description = """Implemented a meteorological fire danger index in
  GRASS GIS based on weather forescasts."""
--->

