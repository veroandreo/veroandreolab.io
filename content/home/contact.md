---
# An instance of the Contact widget.
widget: contact

# This file represents a page section.
headless: true

# Order that this section appears on the page.
weight: 130

title: Contact
subtitle:

content:
  # Automatically link email and phone or display as text?
  autolink: true

  # Email form provider
  form:
    provider: 
    formspree:
      id:
    netlify:
      # Enable CAPTCHA challenge to reduce spam?
      captcha: false

  # Contact details (edit or remove options as required)
  email: veroandreo@gmail.com
  # phone: 888 888 88 88
  address:
    street: Ruta Provincial C45 Km 8
    city: Falda del Cañete 
    region: Córdoba
    postcode: '5187'
    country: 'Argentina'
    country_code: AR
  coordinates:
    latitude: '-31.5204194'
    longitude: '-64.4653258'
  directions: 
  office_hours: 
    - 'Mon to Fri 08:30 to 17:30'
  appointment_url: ''
design:
  columns: '2'
---
