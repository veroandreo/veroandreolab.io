+++
title = "Estimating Hantavirus Risk in Southern Argentina: A GIS-based Approach Combining Human Cases and Host Distribution"
date = "2014-01-01"
authors = ["V. Andreo", "M. Neteler", "D. Rocchini", "M. C. Provensal", "S. Levis", "X. Porcasi", "A. Rizzoli", "M. Lanfri", "M. Scavuzzo", "N. Pini", "D. Enria", "J. Polop"]
publication_types = ["2"]
publication = "Viruses, (6), 1, _pp. 201--222_"
publication_short = ""
abstract = "We use a Species Distribution Modeling (SDM) approach along with Geographic Information Systems (GIS) techniques to examine the potential distribution of hantavirus pulmonary syndrome (HPS) caused by Andes virus (ANDV) in southern Argentina and, more precisely, define and estimate the area with the highest infection probability for humans, through the combination with the distribution map for the competent rodent host (Oligoryzomys longicaudatus). Sites with confirmed cases of HPS in the period 1995–2009 were mostly concentrated in a narrow strip (~90 km × 900 km) along the Andes range from northern Neuquén to central Chubut province. This area is characterized by high mean annual precipitation (~1,000 mm on average), but dry summers (less than 100 mm), very low percentages of bare soil (~10% on average) and low temperatures in the coldest month (minimum average temperature −1.5 °C), as compared to the HPS-free areas, features that coincide with sub-Antarctic forests and shrublands (especially those dominated by the invasive plant Rosa rubiginosa), where rodent host abundances and ANDV prevalences are known to be the highest. Through the combination of predictive distribution maps of the reservoir host and disease cases, we found that the area with the highest probability for HPS to occur overlaps only 28% with the most suitable habitat for O. longicaudatus. With this approach, we made a step forward in the understanding of the risk factors that need to be considered in the forecasting and mapping of risk at the regional/national scale. We propose the implementation and use of thematic maps, such as the one built here, as a basic tool allowing public health authorities to focus surveillance efforts and normally scarce resources for prevention and control actions in vast areas like southern Argentina."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Hantavirus", "SDM", "ENM", "Argentina", "GIS", "MaxEnt", "GRASS GIS"]
url_pdf = "https://doi.org/10.3390/v6010201"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
