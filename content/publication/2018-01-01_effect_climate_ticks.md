+++
title = "Effect of Climate and Land Use on the Spatio-Temporal Variability of Tick-Borne Bacteria in Europe"
date = "2018-01-01"
authors = ["R. Rosa", "V. Andreo", "V. Tagliapietra", "I. Barakova", "D. Arnoldi", "H. Hauffe", "M. Manica", "F. Rosso", "L. Blanarova", "M. Bona", "M. Derdakova", "Z. Hamsikova", "M. Kazimirova", "J. Kraljik", "E. Kocianova", "L. Mahrikova", "L. Minichova", "L. Mosansky", "M. Slovak", "M. Stanko", "E. Spitalska", "E. Ducheyne", "M. Neteler", "Z. Hubalek", "I. Rudolf", "K. Venclikova", "C. Silaghi", "E. Overzier", "R. Farkas", "G. Foldvari", "S. Hornok", "N. Takacs", "A. Rizzoli"]
publication_types = ["2"]
publication = "International Journal of Environmental Research and Public Health, (15), 4, _pp. 732_"
publication_short = ""
abstract = "The incidence of tick-borne diseases caused by _Borrelia burgdorferi_ sensu lato, _Anaplasma phagocytophilum_ and _Rickettsia_ spp. has been rising in Europe in recent decades. Early pre-assessment of acarological hazard still represents a complex challenge. The aim of this study was to model Ixodes ricinus questing nymph density and its infection rate with B. burgdorferi s.l., A. phagocytophilum and Rickettsia spp. in five European countries (Italy, Germany, Czech Republic, Slovakia, Hungary) in various land cover types differing in use and anthropisation (agricultural, urban and natural) with climatic and environmental factors (Normalized Difference Vegetation Index (NDVI), Normalized Difference Water Index (NDWI), Land Surface Temperature (LST) and precipitation). We show that the relative abundance of questing nymphs was significantly associated with climatic conditions, such as higher values of NDVI recorded in the sampling period, while no differences were observed among land use categories. However, the density of infected nymphs (DIN) also depended on the pathogen considered and land use. These results contribute to a better understanding of the variation in acarological hazard for Ixodes ricinus transmitted pathogens in Central Europe and provide the basis for more focused ecological studies aimed at assessing the effect of land use in different sites on tick–host pathogens interaction."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["MODIS", "ticks", "remote sensing", "GRASS GIS", "Europe"]
url_pdf = "https://doi.org/10.3390/ijerph15040732"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
