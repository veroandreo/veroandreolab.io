+++
title = "A novel method based on time series satellite data analysis to detect algal blooms"
date = "2020-06-27"
authors = ["A. German", "V. Andreo", "C. Tauro", "C. M. Scavuzzo", "A. Ferral"]
publication_types = ["2"]
publication = "Ecological Informatics, (59), _101131_"
publication_short = ""
abstract = "Water bodies eutrophication is a worldwide environmental problem characterized by excessive phytoplankton growth which often includes occurrence of harmful algal blooms events. Chlorophyll-a concentration is widely used as an indicator of biomass and can be quantified by optic sensors. In this work, we use a satellite derived Chlorophyll-a concentration time series for the period 2001–2014 obtained from MODIS/TERRA data to detect and characterize algal bloom events in an eutrophic reservoir. Our results demonstrate that fixed threshold methods identify too many bloom dates but a dynamic method based on frequencies analysis and statistical approach, performed better because it represents the normal phytoplankton mass and a bloom is identified like a deviation from it. This approach was tested in San Roque reservoir which supplies fresh water to Córdoba city, one of the most populated of Argentina. According to Carlson index values, this is an eutrophic to hypereutrophic water body. Validation of log10 (Chlorophyll-a concentration) derived from satellite model concentration with ground-based measurements data of Chlorophyll-a concentration demonstrates an acceptable error (RMSE = 0.59) considering data distribution. The implementation of the proposed method to identify blooms over a control data set and validated with LANDSAT 8 - OLI images, demonstrates that the approach described is robust and stable over time."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Chlorophyll-a", "MODIS", "time series", "blooms"]
url_pdf = "https://doi.org/10.1016/j.ecoinf.2020.101131"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
