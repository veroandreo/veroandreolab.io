+++
title = "Potential of remote sensing to predict species invasions: A modelling perspective"
date = "2015-01-01"
authors = ["D. Rocchini", "V. Andreo", "M. Forster", "C. X. Garzon-Lopez", "A. P. Gutierrez", "T. W. Gillespie", "H. C. Hauffe", "K. S. He", "B. Kleinschmit", "P. Mairota", "M. Marcantonio", "M. Metz", "H. Nagendra", "S. Pareeth", "L. Ponti", "C. Ricotta", "A. Rizzoli", "G. Schaab", "M. Zebisch", "R. Zorer", "M. Neteler"]
publication_types = ["2"]
publication = "Progress in Physical Geography, (39), _pp. 283--309_"
publication_short = ""
abstract = "Understanding the causes and effects of species invasions is a priority in ecology and conservation biology. One of the crucial steps in evaluating the impact of invasive species is to map changes in their actual and potential distribution and relative abundance across a wide region over an appropriate time span. While direct and indirect remote sensing approaches have long been used to assess the invasion of plant species, the distribution of invasive animals is mainly based on indirect methods that rely on environmental proxies of conditions suitable for colonization by a particular species. The aim of this article is to review recent efforts in the predictive modelling of the spread of both plant and animal invasive species using remote sensing, and to stimulate debate on the potential use of remote sensing in biological invasion monitoring and forecasting. Specifically, the challenges and drawbacks of remote sensing techniques are discussed in relation to: i) developing species distribution models, and ii) studying life cycle changes and phenological variations. Finally, the paper addresses the open challenges and pitfalls of remote sensing for biological invasion studies including sensor characteristics, upscaling and downscaling in species distribution models, and uncertainty of results."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["remote sensing", "SDM", "GIS", "invasive species"]
url_pdf = "https://doi.org/10.1177/0309133315574659"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
