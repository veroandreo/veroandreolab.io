+++
title = "Distribution of Phlebotominae (Diptera: Psychodidade) species and human cases of leishmaniasis in the Province of Corrientes, Argentina"
date = "2022-12-31"
authors = ["M. L. Villarquide", "V. Andreo", "O. D. Salomon", "M. S. Santini"]
publication_types = ["2"]
publication = "Revista de la Sociedad Entomológica Argentina, 81, _pp. 55--63_"
publication_short = ""
abstract = "We updated the distribution of Phlebotominae (Diptera: Psychodidae) species and human cases of leishmaniasis in the Province of Corrientes, Argentina. Evandromyia correalimai (Martins, Coutinho & Lutz, 1970) is a new record for the province, reported in the urban area of Santo Tomé. We include the currently known distribution map of Phlebotominae sandfly species in Corrientes, and the localities where they were recorded, as well as a map of vector species and reported human cases of leishmaniases."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Eco-epidemiology", "sandflies", "South America", "Vector", "Leishmaniasis"]
url_pdf = "https://doi.org/10.25085/rsea.810409"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++