+++
title = "A New Fully Gap-Free Time Series of Land Surface Temperature from MODIS LST Data"
date = "2017-01-01"
authors = ["M. Metz", "V. Andreo", "M. Neteler"]
publication_types = ["2"]
publication = "Remote Sensing, (9), 12, _pp. 1333_"
publication_short = ""
abstract = "Temperature time series with high spatial and temporal resolutions are important for several applications. The new MODIS Land Surface Temperature (LST) collection 6 provides numerous improvements compared to collection 5. However, being remotely sensed data in the thermal range, LST shows gaps in cloud-covered areas. We present a novel method to fully reconstruct MODIS daily LST products for central Europe at 1 km resolution and globally, at 3 arc-min. We combined temporal and spatial interpolation, using emissivity and elevation as covariates for the spatial interpolation. The reconstructed MODIS LST for central Europe was calibrated to air temperature data through linear models that yielded R2 values around 0.8 and RMSE of 0.5 K. This new method proves to scale well for both local and global reconstruction. We show examples for the identification of extreme events to demonstrate the ability of these new LST products to capture and represent spatial and temporal details. A time series of global monthly average, minimum and maximum LST data and long-term averages is freely available for download."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["MODIS", "LST", "GRASS GIS", "GDAL", "remote sensing", "time series"]
url_pdf = "https://doi.org/10.3390/rs9121333"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
