+++
title = "Variables ambientales en la dinámica espacial de Oligoryzomys longicaudatus (huésped del virus andes) en la región noroeste de Chubut, Argentina"
date = "2012-01-01"
authors = ["V. Andreo"]
publication_types = ["2"]
publication = "Mastozoología Neotropical, (19), 1, _pp. 179--181_"
publication_short = "Mastozoología Neotropical, (19), 1, _pp. 179--181_"
abstract = ""
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Hantavirus", "Argentina", "SDM", "Remote sensing", "Habitat use"]
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
