+++
title = "Can reconstructed Land Surface Temperature data from space predict a West Nile virus outbreak?"
date = "2017-01-01"
authors = ["V. Andreo", "M. Metz", "M. Neteler", "R. Rosa", "M. Marcantonio", "C. Billinis", "A. Rizzoli", "A. Papa"]
publication_types = ["1"]
publication = "ISPRS - International Archives of the Photogrammetry, Remote Sensing and Spatial Information Sciences, (XLII-4/W2), _pp. 19--26_"
publication_short = "ISPRS - International Archives of the Photogrammetry, Remote Sensing and Spatial Information Sciences, (XLII-4/W2), _pp. 19--26_"
abstract = "Temperature is one of the main drivers of ecological processes. The availability of temporally and spatially continuous temperature time series is crucial in different research and application fields, such as epidemiology and control of zoonotic diseases. In 2010, several West Nile virus (WNV) outbreaks in humans were observed in Europe, with the largest number of cases recorded in Greece. Human cases continued to occur for four more years. The occurrence of the 2010’s outbreak in Greece has been related to positive anomalies in temperature. Currently available remote sensing time series might provide the temporal and spatial coverage needed to assess this kind of hypothesis. However, the main problem with remotely sensed temperature are the gaps caused by cloud cover. With the objective of testing the former hypothesis, we reconstructed daily MODIS Land Surface Temperature (LST) data and derived several indices that are known or hypothesized to be related to mosquito populations, WNV transmission or risk of disease since they might constitute proxies for favoring or limiting conditions. We present the first results of the comparisons of time series of LST-derived indices among locations with WNV human cases and municipalities with and without reported WNV infection in Greece between 2010 and 2014."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["outbreaks", "West Nile Fever", "MODIS", "LST", "time series"]
url_pdf = "https://doi.org/10.5194/isprs-archives-XLII-4-W2-19-2017"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
