+++
title = "Variables ambientales en la dinámica espacial de Oligoryzomys longicaudatus (huésped del virus Andes) en la región Noroeste de Chubut, Argentina"
date = "2012-01-01"
authors = ["V. Andreo"]
publication_types = ["3"]
publication = ""
publication_short = "PhD thesis. Universidad Nacional de Rio Cuarto. Córdoba, Argentina."
abstract = "In this thesis work I addressed several aspects of the spatial dynamics of _O. longicaudatus_, Andes virus and Hantavirus Pulmonary Syndrome (HPS) that contribute to the understanding of the relationships between virus-host-disease system and the environment. I studied the distribution and abundance of O. longicaudatus populations and the distribution of Andes virus in the Northwest of Chubut province, Argentina. I also studied the rodent assemblage composition and the characteristics of the habitats they inhabit at local, landscape and regional scales. The relationship of occurrence and abundance of O. longicaudatus with climatic and environmental variables at different spatial scales was assessed by generalized linear models. Finally, O. longicaudatus potential distribution and HPS cases occurrence was modeled and mapped as a function of environmental variables for the country scale. Results showed that O. longicaudatus is one of the most abundant species of the rodent assemblage in Northwestern Chubut. The species was found in every region and landscape unit studied, from subantarctic forests through patagonic steppe, dominating forests and ecotone rodent assemblages. The differences we observed, both spatially and temporally, in abundance, community composition and habitat associations may be explained by the climatic and environmental differences recorded among regions and landscape units. At a regional scale, forest and ecotone would be the habitats that favor higher O. longicaudatus abundances, allowing for increased levels of survival and reproduction. At a landscape scale within ecotone, wild rose shrubland would be the optimum habitat, since the species was always captured there, both in dry and wet years, and dominating the rodent assemblage in this landscape unit. The role of the different kinds of forests was not that clear, but it seems likely that it depends on climatic conditions (precipitations). Results of the analyses performed in chapters 4 and 5, showed that a series of climatic and environmental variables can, at least in part, explain the distribution and abundance of O. longicaudatus at different spatial scales (from local scale to geographic distribution of the species at a country scale). The climatic and environmental variables that resulted significant, were consistent through the scales considered and reflected the abundance and environmental differences observed among regions and landscape units. Wild rose cover was one of the most significant variables at a local scale. This plant provides not only food but most likely refuge to individuals of O. longicaudatus and may constitute a very important resource for the rodent host, mainly in dry years. Wild rose seems to play then, a fundamental role in the ecology of this hantavirus host in Southern Argentina and also in the dynamics of the infection and that of the disease. Anti-hantavirus antibodies presence was detected in every region studied, but not in every landscape unit. However, it is not possible to infer that the infection is completely absent from Austrocedrus forests and shrublands without wild rose, since the rodent host was in fact captured in them. It is remarkable that infected animals were found in wild areas of steppe, where, until this study, the presence of Andes virus had not been detected. In conclusion, results from this work show that, at least to some extent, the distribution and abundance of O. longicaudatus, Andes virus reservoir in southern Argentina, might be understood and modeled as a function of climatic and environmental variables recorded at different spatial scales. At the same time, these results highlight the importance of multiscale approaches in the study of ecological phenomena such as distribution and abundance of organisms and provide potentially useful information to establish risk areas for humans and direct control and prevention programs."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Hantavirus", "Argentina", "SDM", "Remote sensing", "Habitat use"]
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
