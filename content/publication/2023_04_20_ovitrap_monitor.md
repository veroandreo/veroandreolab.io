+++
title = "Ovitrap Monitor - Online application for counting mosquito eggs and visualisation toolbox in support of health services"
date = "2023-04-20"
authors = ["C. Hamesse", "V. Andreo", "C. Rodriguez Gonzalez", "C. Beumier", "J. Rubio", "X. Porcasi", "L. Lopez", "C. Guzman", "R. Haelterman", "M. Shimoni", "C. M. Scavuzzo"]
publication_types = ["2"]
publication = "Ecological Informatics, (75), _102105_"
publication_short = ""
abstract = "Ovitraps are a widely used method for mosquito detection and monitoring, especially Aedes mosquitoes. Eggs present in ovitraps must be routinely counted to generate up-to-date information on potential spread of mosquito-borne diseases. This task is tedious, time consuming and prone to errors if done manually by eye counting. In this contribution, we introduce the Ovitrap Monitor, an online open source and user-friendly integrated application that semi-automatically counts mosquito eggs from low-medium resolution mobile phone pictures. A high correlation was found between counts performed manually by a technician and those obtained with the app using an extensive dataset of more than 750 ovitrap pictures. The application features an intuitive user interface and time-series plots and maps to facilitate data flow and speed up evidence-based decision-making within health organisations battling mosquito-borne diseases. Besides being open source, the Ovitrap Monitor is also backed by test data to guarantee its implementation through benchmarking and enforce research in the public health field."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Aedes aegypti", "Dengue", "Computer vision"]
url_pdf = "https://doi.org/10.1016/j.ecoinf.2023.102105"
url_preprint = ""
url_code = "https://gitlab.com/charles.hamesse/ovitrap-monitor-server"
url_dataset = "https://zenodo.org/record/6962536"
url_project = "https://ovitrap-monitor.netlify.app/"
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = "https://ars.els-cdn.com/content/image/1-s2.0-S1574954123001346-gr1.jpg"
caption = "Main components of the egg counting algorithm that is included within Ovitrap Monitor."
+++