+++
title = "Análisis de una serie temporal de clorofila-a a partir de imágenes MODIS de un embalse eutrófico"
date = "2016-01-01"
authors = ["A. German", "C. Tauro", "V. Andreo", "I. Bernasconi", "A. Ferral"]
publication_types = ["1"]
publication = "2016 IEEE Biennial Congress of Argentina (ARGENCON), _pp. 1--6_"
publication_short = "2016 IEEE Biennial Congress of Argentina (ARGENCON), _pp. 1--6_"
abstract = "Eutrophication is a phenomenon that affects many water bodies around the world. San Roque Dam is not an exception since it has been going through a huge increase in algae concentration. In this framework, its monitoring is a key matter, as is the source of water supply most important of Cordoba city. Remote sensing is a fundamental tool to complement traditional in situ monitoring and understand processes that occur in the reservoir. This study presents a semi-empirical algorithm obtained from MODIS data and in situ chlorophyll-a measurements, provided by the Ministry of Water Resources of Cordoba Province. From this model, a daily time chlorophyll-a time series was obtained from 2001 to 2014. Statistical analysis of the series indicate that water quality of the reservoir has worsened when comparing sets of last (2014–2010) and first (2010–2014) five years."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = []
url_pdf = "https://doi.org/10.1109/ARGENCON.2016.7585365"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
