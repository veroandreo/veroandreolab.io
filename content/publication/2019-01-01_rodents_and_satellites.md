+++
title = "Rodents and satellites: Predicting mice abundance and distribution with Sentinel-2 data"
date = "2019-01-01"
authors = ["V. Andreo", "M. Belgiu", "D. Brito Hoyos", "F. Osei", "C. Provensal", "A. Stein"]
publication_types = ["2"]
publication = "Ecological Informatics, (51), _pp. 157--167_"
publication_short = ""
abstract = "Remote sensing data is widely used in numerous ecological applications. The Sentinel-2 satellites (S2 A and B), recently launched by the European Spatial Agency's (ESA), provides  at present the best revisit time, spatial and spectral resolution among the freely available remote sensing optical data. In this study, we explored the potential of S2 enhanced spectral and spatial resolution to explain and predict mice abundances and distribution in border habitats of agroecosystems. We compared the predictive ability of different vegetation and water indices derived from S2 and Landsat 8 (L8) imagery. Our analyses revealed that the best predictor of mice abundance was L8-derived Enhanced Vegetation Index (EVI). S2-based indices, however, outperformed those computed from L8 bands for indices estimated simultaneously to mice trappings and for mice distribution models. Furthermore, indices including S2 red-edge bands were the best predictors of the distribution of the two most common rodent species in the ensemble. The findings of this study can be used as guidelines when selecting the sensors and vegetation variables to be included in more complex models aimed at predicting the distribution and risk of various vector-borne diseases, and especially rodents in other agricultural landscapes."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Sentinel-2", "Landsat-8", "GRASS GIS", "remote sensing", "rodents"]
url_pdf = "https://doi.org/10.1016/j.ecoinf.2019.03.001"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++