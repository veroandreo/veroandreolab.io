+++
title = "Identifying favorable spatio-temporal conditions for West Nile Virus outbreaks by co-clustering of MODIS LST indices time series"
abstract = "This study presents the ﬁrst results of the use of co-clustering to identify potential spatial and temporal concurrences of favourable conditions for the emergence and maintenance of West Nile Virus (WNV) in Greece. We applied the Bregman block average co-clustering algorithm with I-divergence to various time series (from 2003 to 2016) of indices derived from Land Surface Temperature (LST) reconstructed from MODIS products. The results show that the combination of two temporal and three spatial groups performs best in identifying times and areas with and without WNV human cases, yielding smaller standard deviations in co-clusters. Among the indices that appeared to perform better we found number of summer days, annual average of mean and maximum LST, potential number of mosquito and virus cycles (EIP) and mean LST of the WNV transmission season. These variables are consistent with known effects of temperature over mosquito development and reproduction as well as virus ampliﬁcation. Further research will be carried out to identify groups of variables that cluster both in space and time."
authors = ["V. Andreo", "E. Izquierdo-Verdiguier", "R. Zurita-Milla", "R. Rosa", "A. Rizzoli", "A. Papa"]
date = "2018-07-01T00:00:00Z"
doi = []
featured = true
projects = []
publication = "2018 IEEE International Geoscience and Remote Sensing Symposium, _pp. 4670--4673_"
publication_types = ["1"]
publishDate = "2018-07-01T00:00:00Z"
summary = ""
tags = ["remote sensing", "MODIS", "LST", "time series", "outbreaks"]
url_code = ""
url_dataset = ""
url_pdf = "https://doi.org/10.1109/IGARSS.2018.8519542"
url_poster = ""
url_project = ""
url_slides = ""
url_source = ""
url_video = ""
[header]
image = "featured.png"
caption = 'Co-clustering flowchart'
+++