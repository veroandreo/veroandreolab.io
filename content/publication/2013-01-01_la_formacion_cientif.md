+++
title = "La formación científica y el campo profesional en las Ciencias Biológicas: una propuesta educativa en la universidad."
date = "2013-01-01"
authors = ["A. S. Rivarosa", "A. L. Correa", "V. Andreo"]
publication_types = ["2"]
publication = "Revista de Educación en Biología, (15), 2, _pp. 69--81_"
publication_short = "Revista de Educación en Biología, (15), 2, _pp. 69--81_"
abstract = "In this paper we share a didactic proposal that we carried out in an introductory subject of the Biological Sciences course of studies. With this proposal we attempt to assess the understanding of Biology study field from the perspective of scientific knowledge praxis and the dilemmas faced by the professional profile of biologists in their work environment. We describe some arguments which validate the importance of including the analysis of the nature of scientific knowledge for the formation of science students. Moreover, we justify the thematic axes and didactic activities developed during the course, offering some reflections, explanations and responses from the main actors (both teachers and students) and analyzing the cognitive and conceptual value of the proposed didactic path. In this paper we share a didactic proposal that we carried out in an introductory subject of the Biological Sciences course of studies. With this proposal we attempt to assess the understanding of Biology study field from the perspective of scientific knowledge praxis and the dilemmas faced by the professional profile of biologists in their work environment. We describe some arguments which validate the importance of including the analysis of the nature of scientific knowledge for the formation of science students. Moreover, we justify the thematic axes and didactic activities developed during the course, offering some reflections, explanations and responses from the main actors (both teachers and students) and analyzing the cognitive and conceptual value of the proposed didactic path."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = []
url_pdf = ""
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
