+++
title = "Design and implementation of an operational meteo fire risk forecast based on open source geospatial technology"
date = "2015-01-01"
authors = ["L. M. Bellis", "V. Andreo", "A. Lighezzolo", "J. P. Argañaraz", "S. Lanfri", "K. Clemoveki", "C. M. Scavuzzo"]
publication_types = ["1"]
publication = "Proceedings of the 2015 IEEE International Geoscience and Remote Sensing Symposium (IGARSS), _pp. 2155--2158_"
publication_short = ""
abstract = "We designed an integrated platform for early prediction of meteorological fire risk. The system is operative since the end of 2014 and estimates the fire danger index automatically, based on the 72 hours forecast of the Weather Research and Forecast (WRF) model. Though the system is in experimental phase, the first results showed a quite acceptable performance. Moreover, this index is capable of continuously improving the algorithms to produce enhanced risk estimation. Thus, in the short term, the system would also include geospatial information and satellite based products to help firefighting activities during all phases: early warning, pre-disaster planning, preparing and forecasting, response and assistance, recovering and reconstruction throughout a web map service."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Fire risk", "operational", "open source"]
url_pdf = "https://doi.org/10.1109/IGARSS.2015.7326230"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++
