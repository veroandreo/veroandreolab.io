+++
title = "Spatial analysis of Aedes aegypti activity for public health surveillance"
date = "2019-09-01"
authors = ["X. Porcasi", "V. Andreo", "E. Aguirre", "N. Rojas", "J. Rubio", "N. Frutos", "C. Guzman", "L. Lopez"]
publication_types = ["1"]
publication = "2019 XVIII Workshop on Information Processing and Control (RPIC), _pp. 214--217_"
publication_short = "2019 XVIII Workshop on Information Processing and Control (RPIC), _pp. 214--217_"
abstract = "In Córdoba city the surveillance of Aedes aegypti, the vector of Dengue, Zika and Chikungunya in Argentina, has been done regularly since 2011. Even there are general recommendations at international level about mosquito monitoring, its implementation in a operative way, arises with several questions about how to do, and how to interpret the output of Aedes aegypti sampling/monitoring. The last strategy introduced for Zoonosis Direction of Córdoba is the ovitrap sampling. This work aims to analyze spatially 300 traps distributed over the city, in the past 5 months of monitoring. Global and local indexes of spatial correlation are used over the 5 months. The high activity clusters detected converge in the same area and they are also analyzed in terms of urban cover types derived by Sentinel 2 images. This results provides useful information to control activities and to contribute in the optimization of surveillance strategy."
abstract_short = ""
image_preview = ""
selected = false
projects = []
tags = ["Aedes aegypti", "Dengue Fever", "spatial clustering"]
url_pdf = "https://doi.org/10.1109/RPIC.2019.8882135"
url_preprint = ""
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""
url_poster = ""
url_source = ""
math = true
highlight = true
[header]
image = ""
caption = ""
+++