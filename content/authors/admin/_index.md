---
authors:
- admin
bio: My research interests include remote sensing and GIS applied to public health problems.
education:
  courses:
  - course: PhD in Biological Sciences
    institution: National University of Río Cuarto
    year: 2012
  - course: MSc in Spatial Applications for Early Warning and Response to Emergencies
    institution: National University of Córdoba
    year: 2015
  - course: Biologist (5-year course of studies)
    institution: National University of Río Cuarto
    year: 2005
email: "veroandreo@gmail.com"
interests:
- Remote sensing time series
- Image analysis
- GIS
- SDM
- Vector-borne diseases
- Free and Open Source Software
title: Veronica Andreo
organizations:
- name: Instituto Gulich
  url: "http://ig.conae.unc.edu.ar/"
- name: Consejo Nacional de Investigaciones Cientificas y Tecnicas (CONICET)
  url: "https://www.conicet.gov.ar/"
role: Researcher and Lecturer
social:
- icon: envelope
  icon_pack: fas
  link: '#contact'
- icon: google-scholar
  icon_pack: ai
  link: https://scholar.google.com/citations?user=zoN4MO4AAAAJ&hl=en
- icon: researchgate
  icon_pack: ai
  link: https://www.researchgate.net/profile/Veronica_Andreo2
- icon: orcid
  icon_pack: ai
  link: https://orcid.org/0000-0002-4633-2161
- icon: github
  icon_pack: fab
  link: https://github.com/veroandreo
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/veroandreo
superuser: true
user_groups:
- Researchers
- Visitors
---

I am a biologist and I hold a PhD in Biological Sciences and an MSc in Remote 
Sensing and GIS applications. I work as a researcher for
[CONICET](https://www.conicet.gov.ar/) at
[Gulich Institute](http://ig.conae.unc.edu.ar/) - Argentinian Space
Agency [(CONAE)](https://www.argentina.gob.ar/ciencia/conae) in Córdoba,
Argentina.
My research is focused on uncovering **environmental drivers of vector-borne disease outbreaks**. 
I am mostly interested in those environmental features that can be derived 
**by means of satellite image analysis, remote sensing time series and GIS-based techniques**. 

I am part of the [GRASS GIS](https://grass.osgeo.org/) Development team and I 
serve as [PSC chair](https://trac.osgeo.org/grass/wiki/PSC) since 2021. 
I am a strong advocate for [OSGeo](https://www.osgeo.org/) and free and open 
source software for geo-spatial (FOSS4G). I teach GRASS GIS courses and
workshops regularly. Among other things, I have served as Program Committee 
chair for [**FOSS4G 2021**](2021.foss4g.org/) and volunteered as a mentor for 
GRASS GIS in [Google Code-In](https://codein.withgoogle.com/) and 
[Google Summer of Code](https://summerofcode.withgoogle.com/). 
In 2024, I was a visiting scholar at the 
[Center for Geospatial Analytics](https://cnr.ncsu.edu/geospatial/) (NCSU) within
an [NSF funded project](https://grass.osgeo.org/news/2023_09_06_nsf_grant_awarded/) 
to bolster and broaden the software ecosystem of GRASS GIS. 
